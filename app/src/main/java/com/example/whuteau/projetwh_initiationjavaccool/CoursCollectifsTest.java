package com.example.whuteau.projetwh_initiationjavaccool;
import junit.framework.TestCase;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by whuteau on 27/11/2017.
 */
public class CoursCollectifsTest extends TestCase{

    public CoursCollectifsTest(String testMethodName) {
        super(testMethodName);
    }

    public void testAjoutParticipant() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Participant p = new Participant("Huteau", "William", sdf.parse("06/06/1997"), (byte) 49);
        Participant p1 = new Participant("Herault", "Manon", sdf.parse("23/07/1996"), (byte) 49);
        Participant p2 = new Participant("Pichaud", "Antoine", sdf.parse("10/08/1998"), (byte) 49);
        Participant p3 = new Participant("Jedrqzzak", "Valentin", sdf.parse("02/01/1998"), (byte) 49);
        CoursCollectifs c = new CoursCollectifs("Ski débutant", sdf.parse("15/12/2015"), "13 heures", 6);
        c.ajouterParticipant(p);
        c.ajouterParticipant(p1);
        c.ajouterParticipant(p2);
        c.ajouterParticipant(p3);
        ArrayList<Participant> listeParticipant = c.getLesParticipantsInscrits();
        assertEquals("Insertion non effectuée", listeParticipant.get(0).getNom(), ("Huteau"));
        assertEquals("Le nombre de participants maximum a été dépassé", listeParticipant.size() < c.getNbMaxCours(), true);
    }

    public void testSupprimerParticipant() throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Participant p = new Participant("Huteau", "William", sdf.parse("06/06/1997"), (byte) 49);
        Participant p1 = new Participant("Herault", "Manon", sdf.parse("23/07/1996"), (byte) 49);
        Participant p2 = new Participant("Pichaud", "Antoine", sdf.parse("10/08/1998"), (byte) 49);
        Participant p3 = new Participant("Jedrqzzak", "Valentin", sdf.parse("02/01/1998"), (byte) 49);
        CoursCollectifs c = new CoursCollectifs("Ski débutant", sdf.parse("15/12/2015"), "13 heures", 6);
        c.ajouterParticipant(p);
        c.ajouterParticipant(p1);
        c.ajouterParticipant(p2);
        c.ajouterParticipant(p3);
        assertEquals("Le participant n'est pas supprimé !", c.getLesParticipantsInscrits().size(), 4);
        c.supprimerParticipant(p);
        assertEquals("Le participant n'est pas supprimé !", c.getLesParticipantsInscrits().size(), 3);
        c.supprimerParticipant(p1);
        assertEquals("Le participant n'est pas supprimé !", c.getLesParticipantsInscrits().size(), 2);
        c.supprimerParticipant(p2);
        assertEquals("Le participant n'est pas supprimé !", c.getLesParticipantsInscrits().size(), 1);
        c.supprimerParticipant(p3);
        assertEquals("Le participant n'est pas supprimé !", c.getLesParticipantsInscrits().size(), 0);
    }

    public void testTrouveParticipant() throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Participant p = new Participant("Huteau", "William", sdf.parse("06/06/1997"), (byte) 49);
        Participant p1 = new Participant("Herault", "Manon", sdf.parse("23/07/1996"), (byte) 49);
        Participant p2 = new Participant("Huteau", "Antoine", sdf.parse("10/08/1998"), (byte) 49);
        Participant p3 = new Participant("Jedrqzzak", "Valentin", sdf.parse("02/01/1998"), (byte) 49);
        CoursCollectifs c = new CoursCollectifs("Ski débutant", sdf.parse("15/12/2015"), "13 heures", 6);
        c.ajouterParticipant(p);
        c.ajouterParticipant(p1);
        c.ajouterParticipant(p2);
        c.ajouterParticipant(p3);

        ArrayList<Participant> TrouverPart = new ArrayList<Participant>();


        // 1 participant
        TrouverPart = c.TrouveParticipants("Herault");
        assertEquals("Participant(s) trouvé(s) !", TrouverPart.size(), 1);

        TrouverPart.clear();

        // 2 participants
        TrouverPart = c.TrouveParticipants("Huteau");
        assertEquals("Participant(s) trouvé(s) !", TrouverPart.size(), 2);

    }
}
