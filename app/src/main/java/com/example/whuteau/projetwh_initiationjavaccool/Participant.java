package com.example.whuteau.projetwh_initiationjavaccool;

import java.util.Date;

/**
 * Created by whuteau on 06/11/2017.
 */
public class Participant {

    private String nom;
    private String prenom;
    private Date dateNaissance;
    private Byte numDepartement;

    public Participant() {

    }

    public Participant(String n, String p, Date dateN, Byte numD) {
        nom = n;
        prenom = p;
        dateNaissance = dateN;
        numDepartement = numD;

    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public Byte getNumDepartement() {
        return numDepartement;
    }

    public void setNumpDepartement(Byte value) {
        numDepartement = value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(nom).append("\n").append(prenom).append("\n").append(dateNaissance).append("\n").append(numDepartement);
        return sb.toString();
    }
}
