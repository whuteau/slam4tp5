package com.example.whuteau.projetwh_initiationjavaccool;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by whuteau on 13/11/2017.
 */
public class CoursCollectifs extends Cours {

        //Parametres
    private int nbMaxCours;

        //Accesseurs
    public int getNbMaxCours() {
        return nbMaxCours;
    }

    public void setNbMaxCours(int nbMaxCours) {
        this.nbMaxCours = nbMaxCours;
    }


        //Constructeur
    public CoursCollectifs(String intitule, Date date, String heure, int nbMax){
        super(intitule, date, heure);
        nbMaxCours = nbMax;

    }

        //Methodes
    public boolean ajouterParticipant(Participant unP){
        if(lesParticipantsInscrits.size() < nbMaxCours){
            lesParticipantsInscrits.add(unP);
            return true;
        }
        else {
            return false;
        }
    }

    public void supprimerParticipant(Participant unP) throws Exception{
        boolean present = false;
        present = lesParticipantsInscrits.remove(unP);
        if(present == false){
            throw new Exception("Participant non supprimé");
        }
    }

    public String listeDesParticipants(){
        StringBuilder liste = new StringBuilder();
        for(Participant P : lesParticipantsInscrits){
            liste.append(P.toString()).append("\n");
        }
        return liste.toString();
    }

    public ArrayList<Participant> TrouveParticipants(String unNomFamille){
        ArrayList<Participant>lesParticipantsTrouves = new ArrayList<Participant>();
        for (Participant P : lesParticipantsInscrits){
            if(P.getNom().equals(unNomFamille)){
                lesParticipantsTrouves.add(P);
            }
        }
        return lesParticipantsTrouves;
    }

    @Override
    public String toString() {
        StringBuilder informations = new StringBuilder();
        informations.append(intituleCours).append("\n").append(dateCours).append("\n").append(heureDebutCours).append("\n").append(nbMaxCours).append("\n").append(listeDesParticipants());
        return informations.toString();
    }

}

