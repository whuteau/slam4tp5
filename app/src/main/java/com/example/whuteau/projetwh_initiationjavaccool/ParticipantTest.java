package com.example.whuteau.projetwh_initiationjavaccool;

import junit.framework.TestCase;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by whuteau on 13/11/2017.
 */

public class ParticipantTest extends TestCase {

    public ParticipantTest(String testMethodName) {
        super(testMethodName);
    }

    public void testGetNom() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Participant p = new Participant("Huteau", "William", sdf.parse("06/06/1997"), (byte) 49);
        String n = p.getNom();
        assertEquals("Mauvais nom de participant", n, "Huteau");
    }

    public void testGetPrenom() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Participant p = new Participant("Huteau", "William", sdf.parse("06/06/1997"), (byte) 49);
        String pre = p.getPrenom();
        assertEquals("Mauvais prénom de participant", pre, "William");
    }

    public void testGetDateNaissance() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Participant p = new Participant("Huteau", "William", sdf.parse("06/06/1997"), (byte) 49);
        Date dateN = p.getDateNaissance();
        assertEquals("Mauvaise date de naissance du participant", dateN, sdf.parse("06/06/1997"));
    }

    public void testGetNumDepartement() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Participant p = new Participant("Huteau", "William", sdf.parse("06/06/1997"), (byte) 49);
        Byte numD = p.getNumDepartement();
        Byte departement = (byte) 49;
        assertEquals("Mauvais numéro de département du participant", numD, departement);
    }

    // reparer cette methode !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    public void testToString() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Participant p = new Participant("Huteau", "William", sdf.parse("06/06/1997"), (byte) 49);
        String test = p.toString();
        assertEquals("Mauvais string", test, p.getNom() + "\n" + p.getPrenom() + "\n" + (p.getDateNaissance()) + "\n" + p.getNumDepartement());
    }
}
