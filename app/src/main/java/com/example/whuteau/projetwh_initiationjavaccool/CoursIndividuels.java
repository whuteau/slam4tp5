package com.example.whuteau.projetwh_initiationjavaccool;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by whuteau on 13/11/2017.
 */
public class CoursIndividuels extends Cours{

        //Parametres

    private Participant p;
    private Participant m;

        //Accesseurs
    public Participant getM() {
            return m;
        }

    public void setM(Participant m) {
        this.m = m;
    }

    public Participant getP() {
        return p;
    }

    public void setP(Participant p) {
        this.p = p;
    }

        //Constructeur
    public CoursIndividuels(String intitule, Date date, String heure, Participant M){
        super(intitule, date, heure);
        this.m = M;

    }

        //Methodes
    public boolean ajouterParticipant(Participant unP){
        if(p == null){
            p = unP;
            return true;}
        {
            return false;
        }

    };

    public void supprimerParticipant(Participant unP) throws Exception{
        if(p == unP){
            p = null;
        }
    };

    public String listeDesParticipants(){
        StringBuilder liste = new StringBuilder();
        if(p != null){
            liste.append(p);
        }
        else{
            liste.append("aucun participant affecté à ce cours");
        }
        return liste.toString();
    };

    public ArrayList<Participant> TrouveParticipants(String unNomFamille){
        ArrayList<Participant>lesParticipantsTrouves = new ArrayList<Participant>();
        for (Participant P : lesParticipantsInscrits){
            if(P.getNom().equals(unNomFamille)){
                lesParticipantsTrouves.add(P);
            }
        }
        return lesParticipantsTrouves;
    };

    @Override
    public String toString(){
        StringBuilder informations = new StringBuilder();
        informations.append(m).append("\n").append(p);
        return informations.toString();
    };

}

